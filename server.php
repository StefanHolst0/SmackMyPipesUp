<?php
class Database{
    private $host = "localhost";
    private $db_name = "api_db";
    private $username = "root";
    private $password = "";
    public $conn;
 
    public function getConnection(){
        $this->conn = null;
 
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->conn;
    }
}

class Score{
    private $conn;
    private $table_name = "score";
 
    public $id;
    public $name;
    public $score;
    public $mode;
 
    public function __construct($db){
        $this->conn = $db;
    }

    function read(){
        $query = "SELECT * FROM score";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
     
        return $stmt;
    }

    function insert($name, $score, $mode){
        $query = "INSERT INTO score (name, score, mode) VALUES (" + $name + ", " + $score + ", " + $mode + ")";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
    }
}

$database = new Database();
$db = $database->getConnection();
$score = new Score($db);

switch($_SERVER['REQUEST_METHOD'])
{
    case 'GET': 
        getHighScores();
        break;
    case 'POST': 
        pushHighScore(); 
        break;
}

function getHighScores(){
    $stmt = $score->read();
    $num = $stmt->rowCount();

    if($num>0){
        $scores_arr=array();
        $scores_arr["score"]=array();
    
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
    
            $score_item=array(
                "id" => $id,
                "name" => $name,
                "score" => $score,
                "mode" => $mode
            );
    
            array_push($scores["score"], $score_item);
        }
    
        echo json_encode($scores_arr);
    }
}
function pushHighScore(){
    echo $_SERVER["argv"];
    // $smtm = $score->insert("test", "test", "test");
    echo "inserted";
}